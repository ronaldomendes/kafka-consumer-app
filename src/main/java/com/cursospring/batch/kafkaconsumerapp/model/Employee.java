package com.cursospring.batch.kafkaconsumerapp.model;

import lombok.Data;

@Data
public class Employee {

    private String employeeId;
    private String firstName;
    private String lastName;
    private String email;
    private int age;
}
