package com.cursospring.batch.kafkaconsumerapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.cursospring.batch.kafkaconsumerapp.service"})
public class KafkaConsumerAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(KafkaConsumerAppApplication.class, args);
    }

}
