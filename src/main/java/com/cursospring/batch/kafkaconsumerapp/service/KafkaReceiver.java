package com.cursospring.batch.kafkaconsumerapp.service;

import com.cursospring.batch.kafkaconsumerapp.model.Employee;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.concurrent.CountDownLatch;

@Component
@Slf4j
public class KafkaReceiver {

    private CountDownLatch latch = new CountDownLatch(1);

    @KafkaListener(topics = "TOPIC_TEST")
    public void receive(Employee employee) {
        log.info("Received employee details: {}", employee.toString());
        latch.countDown();
    }
}
